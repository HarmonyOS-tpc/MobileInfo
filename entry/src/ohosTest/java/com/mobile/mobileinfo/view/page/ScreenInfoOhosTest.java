package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ScreenInfoOhosTest {
    ScreenInfo mScreenInfo;
    List<Param> list;

    @Test
    public void getList() {
        list = mScreenInfo.getList();
        assertTrue(Float.parseFloat(getParamValue(list, BaseData.Screen.DENSITY_SCALE)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Screen.DENSITY_DPI)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Screen.WIDTH)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Screen.HEIGHT)) >= 0);
        assertTrue(getParamValue(list, BaseData.Screen.IS_SCREEN_AUTO).length() >= 0);
        assertTrue(getParamValue(list, BaseData.Screen.IS_SCREEN_AUTO_CHANGE).length() >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Screen.SCREEN_BRIGHTNESS)) >= 0);
        assertTrue(getParamValue(list, BaseData.Screen.CHECK_HIDE_STATUSBAR).length() >= 0);
    }

    @Test
    public void getName() {
        assertEquals("ScreenInfo", mScreenInfo.getName());
    }

    @Before
    public void setUp() {
        mScreenInfo = new ScreenInfo();
    }
}