package com.mobile.mobileinfo.view.page;

import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CameraInfoOhosTest {
    CameraInfo mCameraInfo;
    List<Param> list;

    @Test
    public void getList() {
        list = mCameraInfo.getList();
        assertTrue(list.size() > 0);
    }

    @Test
    public void getName() {
        assertEquals("CameraInfo", mCameraInfo.getName());
    }

    @Before
    public void setUp() {
        mCameraInfo = new CameraInfo();
    }
}