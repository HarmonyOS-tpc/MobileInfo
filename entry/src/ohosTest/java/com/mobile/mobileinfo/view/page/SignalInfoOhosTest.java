package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SignalInfoOhosTest {
    SignalInfo msignalInfo;
    List<Param> list;

    @Before
    public void setUp() {
        msignalInfo = new SignalInfo();
    }

    @Test
    public void getName() {
        assertEquals("SignalInfo", msignalInfo.getName());
    }

    @Test
    public void getList() {
        list = msignalInfo.getList();
        assertTrue(getParamValue(list, BaseData.Signal.TYPE).length() > 0);
        assertTrue(getParamValue(list, BaseData.Signal.BSSID).length() > 0);
        assertTrue(getParamValue(list, BaseData.Signal.SSID).length() > 0);
        assertTrue(getParamValue(list, BaseData.Signal.N_IP_ADDRESS).length() > 0);
        assertTrue(getParamValue(list, BaseData.Signal.MAC_ADDRESS).length() > 0);
        assertTrue(getParamValue(list, BaseData.Signal.LINK_SPEED).length() > 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Signal.RSSI)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Signal.LEVEL)) >= 0);
        assertTrue(getParamValue(list, BaseData.Signal.SUPPLICANT_STATE).length() > 0);
        assertTrue(getParamValue(list, BaseData.Signal.PROXY).length() > 0);
        assertTrue(getParamValue(list, BaseData.Signal.PROXY_ADDRESS).length() > 0);
        assertTrue(getParamValue(list, BaseData.Signal.PROXY_PORT).length() > 0);

    }
}