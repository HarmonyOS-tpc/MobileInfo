package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BatteryInfoOhosTest {
    BatteryInfo mBatteryInfo;
    List<Param> list;

    @Test
    public void getList() {
        list = mBatteryInfo.getList();
        String chargeval = getParamValue(list, BaseData.Battery.BR);
        int batteryperc;
        if (chargeval.contains("."))
            batteryperc = Integer.parseInt(chargeval.split(".")[0]);
        else
            batteryperc = Integer.parseInt(chargeval.substring(0, chargeval.length()-1));
        assertTrue(batteryperc >= 0 && batteryperc <= 100 );
        assertTrue(getParamValue(list, BaseData.Battery.STATUS).length() > 0);
        assertTrue(getParamValue(list, BaseData.Battery.PLUG_STATE).length() > 0);
        assertTrue(getParamValue(list, BaseData.Battery.HEALTH).length() > 0);
        assertTrue(getParamValue(list, BaseData.Battery.PRESENT).length() > 0);
        assertTrue(getParamValue(list, BaseData.Battery.TECHNOLOGY).length() > 0);
        assertTrue(getParamValue(list, BaseData.Battery.TEMPERATURE).length() > 0);
        assertTrue(getParamValue(list, BaseData.Battery.VOLTAGE).length() > 0);
    }

    @Test
    public void getName() {
        assertEquals("BatteryInfo", mBatteryInfo.getName());
    }

    @Before
    public void setUp() {
        mBatteryInfo = new BatteryInfo();
    }
}