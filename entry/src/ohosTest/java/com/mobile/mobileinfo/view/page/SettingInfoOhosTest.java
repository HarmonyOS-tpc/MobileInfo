package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SettingInfoOhosTest {
    SettingInfo mSettingInfo;
    List<Param> list;

    @Before
    public void setUp() {
        mSettingInfo = new SettingInfo();
    }

    @Test
    public void getName() {
        assertEquals("SettingInfo", mSettingInfo.getName());
    }

    @Test
    public void getList() {
        list = mSettingInfo.getList();
        assertTrue(getParamValue(list, BaseData.Settings.SCREEN_OFF_TIMEOUT).length() > 0);
        assertTrue(getParamValue(list, BaseData.Settings.SOUND_EFFECTS_ENABLED).length() > 0);
        assertTrue(getParamValue(list, BaseData.Settings.SCREEN_BRIGHTNESS_MODE).length() > 0);
        assertTrue(getParamValue(list, BaseData.Settings.DEVELOPMENT_SETTINGS_ENABLED).length() > 0);
        assertTrue(getParamValue(list, BaseData.Settings.ACCELEROMETER_ROTATION).length() > 0);
        assertTrue(getParamValue(list, BaseData.Settings.USB_MASS_STORAGE_ENABLED).length() > 0);
    }
}