package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BuildInfoOhosTest {
    BuildInfo mBuildInfo;
    List<Param> list;

    @Test
    public void getList() {
        list = mBuildInfo.getList();
        assertTrue(getParamValue(list, BaseData.Build.BOARD).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.BOOTLOADER).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.BRAND).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.DEVICE).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.DISPLAY).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.FINGERPRINT).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.HARDWARE).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.ID).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.MANUFACTURER).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.MODEL).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.PRODUCT).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.RADIO).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.SERIAL).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.TAGS).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.TIME).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.TYPE).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.USER).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.OS_VERSION).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.RELEASE_VERSION).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.CODE_NAME).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.INCREMENTAL).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.SDK_INT).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.PREVIEW_SDK_INT).length() > 0);
        assertTrue(getParamValue(list, BaseData.Build.SECURITY_PATCH).length() > 0);
        assertTrue(getParamValue(list, "wifi.interface").length() > 0);
        assertTrue(getParamValue(list, "gsm.sim.state").length() > 0);
        assertTrue(getParamValue(list, "gsm.version.baseband").length() > 0);
        assertTrue(getParamValue(list, "gsm.version.ril-impl").length() > 0);
        assertTrue(getParamValue(list, "net.hostname").length() > 0);
        assertTrue(getParamValue(list, "ro.board.platform").length() > 0);
        assertTrue(getParamValue(list, "ro.build.characteristics").length() > 0);
        assertTrue(getParamValue(list, "ro.build.date").length() > 0);
        assertTrue(getParamValue(list, "ro.build.description").length() > 0);
        assertTrue(getParamValue(list, "ro.boot.serialno").length() > 0);
        assertTrue(getParamValue(list, "ro.opengles.version").length() > 0);
        assertTrue(getParamValue(list, "ro.product.cpu.abi").length() > 0);
        assertTrue(getParamValue(list, "gsm.current.phone-type").length() > 0);
        assertTrue(getParamValue(list, "gsm.network.type").length() > 0);
        assertTrue(getParamValue(list, "gsm.operator.isroaming").length() > 0);
        assertTrue(getParamValue(list, "ro.adb.secure").length() > 0);
        assertTrue(getParamValue(list, "ro.carrier").length() > 0);
        assertTrue(getParamValue(list, "ro.secure").length() > 0);
        assertTrue(getParamValue(list, "ro.debuggable").length() > 0);
        assertTrue(getParamValue(list, "rild.libargs").length() > 0);
        assertTrue(getParamValue(list, "persist.sys.timezone").length() > 0);
        assertTrue(getParamValue(list, "persist.sys.country").length() > 0);
        assertTrue(getParamValue(list, "gsm.operator.numeric").length() > 0);
        assertTrue(getParamValue(list, "wlan.driver.status").length() > 0);
        assertTrue(getParamValue(list, "ro.runtime.firstboot").length() > 0);
        assertTrue(getParamValue(list, "init.svc.adbd").length() > 0);
        assertTrue(getParamValue(list, "ro.serialno").length() > 0);
        assertTrue(getParamValue(list, "keyguard.no_require_sim").length() > 0);
        assertTrue(getParamValue(list, "ro.bt.bdaddr_path").length() > 0);
    }

    @Test
    public void getName() {
        assertEquals("BuildInfo", mBuildInfo.getName());
    }

    @Before
    public void setUp() {
        mBuildInfo = new BuildInfo();
    }
}