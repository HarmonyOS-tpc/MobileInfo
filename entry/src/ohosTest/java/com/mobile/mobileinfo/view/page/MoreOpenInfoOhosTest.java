package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MoreOpenInfoOhosTest {
    MoreOpenInfo mmoreOpenInfo;
    List<Param> list;

    @Before
    public void setUp() {
        mmoreOpenInfo = new MoreOpenInfo();
    }

    @Test
    public void getName() {
        assertEquals("MoreOpenInfo", mmoreOpenInfo.getName());
    }

    @Test
    public void getList() {
        list = mmoreOpenInfo.getList();
        assertTrue(getParamValue(list, BaseData.MoreOpen.CHECK_BY_PRIVATE_FILE_PATH).length() > 0);
        assertTrue(getParamValue(list, BaseData.MoreOpen.CHECK_BY_MULTIAPK_PACKAGE_NAME).length() > 0);
        assertTrue(getParamValue(list, BaseData.MoreOpen.CHECK_BY_HAS_SAMEUID).length() > 0);
        assertTrue(getParamValue(list, "checkLs").length() > 0);
    }
}