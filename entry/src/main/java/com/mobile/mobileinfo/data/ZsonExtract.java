package com.mobile.mobileinfo.data;

import com.mobile.mobileinfo.util.StringUtil;
import ohos.agp.components.element.Element;
import ohos.utils.zson.ZSONArray;
import ohos.utils.zson.ZSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ZsonExtract {
    public static List<Param> getListParam(ZSONObject jsonObject) {
        List<Param> list = new ArrayList<>();
        try {
            Iterator iterator = jsonObject.keySet().iterator();
            while (iterator.hasNext()) {
                Param param = new Param();
                String key = (String) iterator.next();
                param.setKey(key);
                if (jsonObject.get(key) instanceof Element) {
                    param.setValue(jsonObject.get(key));
                } else {
                    param.setValue(StringUtil.formatString(jsonObject.get(key).toString()));
                }
                list.add(param);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<Param> getListParam(String key, Object value) {
        List<Param> list = new ArrayList<>();
        Param param = new Param();
        param.setValue(value + "");
        param.setKey(key);
        list.add(param);
        return list;
    }

    public static List<Param> getListParam(ZSONArray jsonArray) {
        List<Param> list = new ArrayList<>();
        try {
            for (int i = 0; i < jsonArray.size(); i++) {
                Param param = new Param();
                ZSONObject jsonObject = (ZSONObject) jsonArray.get(i);
                if (jsonObject.containsKey("isSystem")) {
                    if (!jsonObject.getBoolean("isSystem")) {
                        param.setValue(jsonObject.get("icon"));
                        jsonObject.remove("icon");
                        String key = StringUtil.formatString(jsonObject.toString());
                        param.setKey(key);
                        list.add(param);
                    }
                } else {
                    param.setKey(i + 1 + "");
                    String value = StringUtil.formatString(jsonObject.toString());
                    param.setValue(value);
                    list.add(param);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
