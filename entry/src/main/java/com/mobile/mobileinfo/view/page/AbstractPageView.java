/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package com.mobile.mobileinfo.view.page;

import com.mobile.mobileinfo.ResourceTable;
import com.mobile.mobileinfo.data.Param;
import com.mobile.mobileinfo.view.model.PageInfo;
import com.mobile.mobileinfo.view.tab.TabInfo;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.window.service.Window;
import ohos.app.Context;

import java.util.List;

import static ohos.agp.components.Component.HIDE;
import static ohos.agp.components.Component.VISIBLE;


/**
 * The type Sample page view.
 */
public abstract class AbstractPageView implements PageInfo, TabInfo {
    private Context context;
    private Window mWindow;

    protected Context getContext() {
        return context;
    }

    protected Window getWindow() {
        return mWindow;
    }

    private Component loadView(Context context, Window window) {
        this.context = context;
        this.mWindow = window;
        return attachList(context);
    }

    private ComponentContainer attachList(Context context) {
        ComponentContainer listcontainer = (ComponentContainer) LayoutScatter.getInstance(context).parse
                (ResourceTable.Layout_list_page, null, false);
        ListContainer list_component = (ListContainer) listcontainer.findComponentById(ResourceTable.Id_ListContainerC);
        list_component.setItemProvider(new MainListProvider(context, getList()));
        return listcontainer;
    }

    abstract List<Param> getList();

    @Override
    public Component getRootView(Context context, Window window) {
        return loadView(context, window);
    }

    public static class MainListProvider extends BaseItemProvider {
        List<Param> params;
        Context context;

        MainListProvider(Context ctx, List<Param> params) {
            this.params = params;
            context = ctx;
        }

        @Override
        public int getCount() {
            return params.size();
        }

        @Override
        public Object getItem(int i) {
            return params.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
            if (component == null) {
                component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_mob_list_item, null, false);
            }
            Param param = params.get(i);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.key = (Text) component.findComponentById(ResourceTable.Id_mob_list_tv_key);
            viewHolder.value = (Text) component.findComponentById(ResourceTable.Id_mob_list_tv_param);
            viewHolder.iv = (Image) component.findComponentById(ResourceTable.Id_mob_list_iv_logo);

            viewHolder.key.setText(param.getKey());
            Object value = param.getValue();
            if (value instanceof Element) {
                viewHolder.iv.setVisibility(VISIBLE);
                viewHolder.iv.setImageElement((Element)value);
                viewHolder.value.setVisibility(HIDE);
            } else {
                viewHolder.iv.setVisibility(HIDE);
                viewHolder.value.setText(param.getValue().toString());
            }
            return component;
        }

        class ViewHolder {
            Text key;
            Text value;
            Image iv;
        }
    }
}
