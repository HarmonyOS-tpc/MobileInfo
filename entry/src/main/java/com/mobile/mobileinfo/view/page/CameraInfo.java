/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobilehardware.camera.CameraHelper;
import com.mobile.mobileinfo.data.Param;

import java.util.List;

import static com.mobile.mobileinfo.data.ZsonExtract.getListParam;

public class CameraInfo extends AbstractPageView{
    @Override
    List<Param> getList() {
        return getListParam(CameraHelper.getCameraInfo().getZSONArray(BaseData.Camera.CAMERA_INFO));
    }

    @Override
    public String getName() {
        return "CameraInfo";
    }
}
