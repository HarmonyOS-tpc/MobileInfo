package com.mobile.mobilehardware.network;

import com.mobile.mobilehardware.utils.DataUtil;
import com.mobile.mobilehardware.utils.Logs;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.app.Context;
import ohos.net.NetCapabilities;
import ohos.net.NetHandle;
import ohos.net.NetHotspot;
import ohos.net.NetManager;
import ohos.nfc.NfcController;
import ohos.sysappcomponents.settings.SystemSettings;
import ohos.telephony.CellularDataInfoManager;
import ohos.utils.zson.ZSONObject;
import ohos.wifi.WifiDevice;

public class NetWorkInfo {
    private static final String TAG = NetWorkInfo.class.getSimpleName();

    static ZSONObject getMobNetWork(Context context) {
        NetWorkBean netWorkBean = new NetWorkBean();
        try {
            netWorkBean.setType(DataUtil.networkTypeALL(context));
            netWorkBean.setNetworkAvailable(DataUtil.isNetworkAvailable(context));
            netWorkBean.setHaveIntent(haveIntent(context));
            netWorkBean.setFlightMode(getAirplaneMode(context));
            netWorkBean.setNFCEnabled(hasNfc(context));
			netWorkBean.setHotspotEnabled(isWifiApEnabled(context));
            netWorkBean.setVpn(getVpnData(context));
            NetManager manager = NetManager.getInstance(context);
            if (manager != null) {
                WifiDevice wifiDevice = WifiDevice.getInstance(context);
                if (wifiDevice != null && wifiDevice.isConnected()) {
                    return netWorkBean.toJSONObject();
                }
            }
            WifiDevice mWifiManager = WifiDevice.getInstance(context);
            if (mWifiManager == null) {
                return netWorkBean.toJSONObject();
            }
        } catch (Exception e) {
            Logs.e(TAG, e.toString());
        }
        return netWorkBean.toJSONObject();
    }
            
    private static boolean getAirplaneMode(Context context) {
        DataAbilityHelper resolver = DataAbilityHelper.creator(context);
        String status = SystemSettings.getValue(resolver, SystemSettings.General.AIRPLANE_MODE_STATUS);
        if (status == null)
            return false;
        return status.equalsIgnoreCase("1");
    }

    private static boolean hasNfc(Context context) {
        boolean bRet = false;
        if (context == null) {
            return false;
        }
        NfcController manager = NfcController.getInstance(context);
        if (manager == null) {
            return false;
        }
        if (manager.isNfcAvailable() && manager.isNfcOpen()){
            bRet = true;
        }
        return bRet;
    }

    /**
     * 热点开关是否打开
     * @param context Application context
     * @return true if hotspot enabled
     */
    private static boolean isWifiApEnabled(Context context) {
        return NetHotspot.getInstance(context).getHotspotIfaces().length > 0;
    }

    /* 是否有数据网络接入
     *
     * @param context
     * @return
     */
    public static boolean haveIntent(Context context) {
        CellularDataInfoManager cdim = CellularDataInfoManager.getInstance(context);
        if (cdim == null)
            return false;
        int slotId = cdim.getDefaultCellularDataSlotId();
        int dataState = cdim.getCellularDataState(slotId);
        return dataState == CellularDataInfoManager.DATA_STATE_CONNECTED;
    }

    public static boolean getVpnData(Context context) {
        try {
            NetManager cm = NetManager.getInstance(context);
            if (cm == null) {
                return false;
            }
            NetHandle netHandle = cm.getDefaultNet();
            NetCapabilities netCapabilities = cm.getNetCapabilities(netHandle);
            if (netCapabilities.hasCap(NetCapabilities.NET_CAPABILITY_VALIDATED)) {
                if (netCapabilities.hasBearer(NetCapabilities.BEARER_VPN)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            // e.printStackTrace();
            return false;
        }
    }
}
