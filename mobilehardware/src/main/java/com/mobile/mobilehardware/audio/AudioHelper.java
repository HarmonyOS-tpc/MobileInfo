package com.mobile.mobilehardware.audio;


import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
public class AudioHelper extends AudioInfo {

    /**
     * 获取电量信息
     *
     * @return 电量JSON
     */
    public static ZSONObject mobGetMobAudio() {
        return getAudio();
    }
}
