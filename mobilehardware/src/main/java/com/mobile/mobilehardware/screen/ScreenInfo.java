package com.mobile.mobilehardware.screen;

import com.mobile.mobilehardware.utils.Logs;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.agp.components.Component;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.agp.window.service.Window;
import ohos.app.Context;
import ohos.sysappcomponents.settings.SystemSettings;
import ohos.utils.zson.ZSONObject;

import static ohos.sysappcomponents.settings.SystemSettings.Display.AUTO_SCREEN_BRIGHTNESS;
import static ohos.sysappcomponents.settings.SystemSettings.Display.SCREEN_BRIGHTNESS_STATUS;
import static ohos.sysappcomponents.settings.SystemSettings.General.ACCELEROMETER_ROTATION_STATUS;

/**
 * @author guxiaonian
 */
class ScreenInfo {
    private static final String TAG = ScreenInfo.class.getSimpleName();

    static ZSONObject getMobScreen(Context context, Window window) {
        final ScreenBean screenBean = new ScreenBean();
        try {
            DisplayAttributes displayMetrics = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
            screenBean.setDensityScale(displayMetrics.densityPixels);
            screenBean.setDensityDpi(displayMetrics.densityDpi);
            screenBean.setWidth(displayMetrics.width);
            screenBean.setHeight(displayMetrics.height);
            DataAbilityHelper dataAbilityHelper = DataAbilityHelper.creator(context);
            int screenMode = Integer.parseInt(SystemSettings.getValue(dataAbilityHelper, AUTO_SCREEN_BRIGHTNESS));
            int screenBrightness = Integer.parseInt(SystemSettings.getValue(dataAbilityHelper, SCREEN_BRIGHTNESS_STATUS));
            int screenChange = Integer.parseInt(SystemSettings.getValue(dataAbilityHelper, ACCELEROMETER_ROTATION_STATUS));
            screenBean.setScreenAuto((screenMode == 1));
            screenBean.setScreenBrightness(screenBrightness);
            screenBean.setScreenAutoChange((screenChange == 1));
            screenBean.setCheckHideStatusBar(checkHideStatusBar(window));
        } catch (Exception e) {
            Logs.i(TAG, e.toString());
        }
        return screenBean.toJSONObject();
    }


    private static boolean checkHideStatusBar(Window window) {
        return window.getStatusBarVisibility() == Component.INVISIBLE;
    }
}