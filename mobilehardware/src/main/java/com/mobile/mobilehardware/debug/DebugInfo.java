package com.mobile.mobilehardware.debug;

import com.mobile.mobilehardware.utils.Logs;
import ohos.app.Context;
import ohos.bundle.ApplicationInfo;
import ohos.hiviewdfx.Debug;
import ohos.os.ProcessManager;
import ohos.utils.zson.ZSONObject;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * @author guxiaonian
 */
class DebugInfo {
    private static final String TAG = DebugInfo.class.getSimpleName();

    /**
     * 调试模式判断
     * @param context Application COntext
     * @return debug data
     */
    static ZSONObject getDebugData(Context context) {
        DebugBean debugBean = new DebugBean();
        try {
            debugBean.setDebugVersion(checkIsDebugVersion(context));
            debugBean.setDebugging(checkIsDebuggerConnected());
            debugBean.setReadProcStatus(readProcStatus());
        } catch (Exception e) {
            Logs.e(TAG, e.toString());
        }
        return debugBean.toJSONObject();
    }

    /**
     * 判斷是debug版本
     *
     * @param context application context
     * @return True if debug version
     */
    private static boolean checkIsDebugVersion(Context context) {
        return context.getApplicationInfo().debug;
    }

    /**
     * 是否正在调试
     *
     * @return True if debug
     */
    private static boolean checkIsDebuggerConnected() {
        try {
            return Debug.getDebuggerConnectStatus();
        } catch (Exception e) {

        }
        return false;
    }

    /**
     * 读取TracePid
     *
     * @return true if proc status
     */
    private static boolean readProcStatus() {
        try {
            BufferedReader localBufferedReader =
                    new BufferedReader(new FileReader("/proc/" + ProcessManager.getPid() + "/status"));
            String tracerPid = "";
            for (; ; ) {
                String str = localBufferedReader.readLine();
                if (str.contains("TracerPid")) {
                    tracerPid = str.substring(str.indexOf(":") + 1, str.length()).trim();
                    break;
                }
                if (str == null) {
                    break;
                }
            }
            localBufferedReader.close();
            if ("0".equals(tracerPid)) {
                return false;
            } else {
                return true;
            }
        } catch (Exception fuck) {
            return false;
        }
    }
}


