/*
 * Copyright (c) 2021-2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobile.mobilehardware.utils;

public class MemoryUnit {
    public static String formatMemoryData(long sizeBytes) {
        final boolean negative = (sizeBytes < 0);
        float result = negative ? -sizeBytes : sizeBytes;
        String unit = "B";
        long mult = 1;
        if (result > 900) {
            unit = "KB";
            mult = 1000;
            result = result / 1000;
        }
        if (result > 900) {
            unit = "MB";
            mult *= 1000;
            result = result / 1000;
        }
        if (result > 900) {
            unit = "GB";
            mult *= 1000;
            result = result / 1000;
        }
        if (result > 900) {
            unit = "TB";
            mult *= 1000;
            result = result / 1000;
        }
        if (result > 900) {
            unit = "PB";
            mult *= 1000;
            result = result / 1000;
        }

        final String roundFormat;
        if (mult == 1 || result >= 100) {
            roundFormat = "%.0f";
        } else {
            roundFormat = "%.2f";
        }
        if (negative) {
            result = -result;
        }
        return String.format(roundFormat, result) + unit;
    }
}
