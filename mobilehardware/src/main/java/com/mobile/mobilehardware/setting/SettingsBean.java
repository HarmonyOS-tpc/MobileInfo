package com.mobile.mobilehardware.setting;

import com.mobile.mobilehardware.base.BaseBean;
import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobilehardware.utils.Logs;
import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
public class SettingsBean extends BaseBean {
    private static final String TAG = SettingsBean.class.getSimpleName();
    private String screenOffTimeout;
    private String soundEffectsEnabled;
    private String screenBrightnessMode;
    private String developmentSettingsEnabled;
    private String accelerometerRotation;
    private String usbMassStorageEnabled;

    public String getUsbMassStorageEnabled() {
        return usbMassStorageEnabled;
    }

    public void setUsbMassStorageEnabled(String usbMassStorageEnabled) {
        this.usbMassStorageEnabled = usbMassStorageEnabled;
    }

    public String getScreenOffTimeout() {
        return screenOffTimeout;
    }

    public void setScreenOffTimeout(String screenOffTimeout) {
        this.screenOffTimeout = screenOffTimeout;
    }

    public String getSoundEffectsEnabled() {
        return soundEffectsEnabled;
    }

    public void setSoundEffectsEnabled(String soundEffectsEnabled) {
        this.soundEffectsEnabled = soundEffectsEnabled;
    }

    public String getScreenBrightnessMode() {
        return screenBrightnessMode;
    }

    public void setScreenBrightnessMode(String screenBrightnessMode) {
        this.screenBrightnessMode = screenBrightnessMode;
    }

    public String getDevelopmentSettingsEnabled() {
        return developmentSettingsEnabled;
    }

    public void setDevelopmentSettingsEnabled(String developmentSettingsEnabled) {
        this.developmentSettingsEnabled = developmentSettingsEnabled;
    }

    public String getAccelerometerRotation() {
        return accelerometerRotation;
    }

    public void setAccelerometerRotation(String accelerometerRotation) {
        this.accelerometerRotation = accelerometerRotation;
    }

    @Override
    protected ZSONObject toJSONObject() {
        try {
            jsonObject.put(BaseData.Settings.SCREEN_OFF_TIMEOUT, isEmpty(screenOffTimeout));
            jsonObject.put(BaseData.Settings.SOUND_EFFECTS_ENABLED, isEmpty(soundEffectsEnabled));
            jsonObject.put(BaseData.Settings.SCREEN_BRIGHTNESS_MODE, isEmpty(screenBrightnessMode));
            jsonObject.put(BaseData.Settings.DEVELOPMENT_SETTINGS_ENABLED, isEmpty(developmentSettingsEnabled));
            jsonObject.put(BaseData.Settings.ACCELEROMETER_ROTATION, isEmpty(accelerometerRotation));
            jsonObject.put(BaseData.Settings.USB_MASS_STORAGE_ENABLED, isEmpty(usbMassStorageEnabled));

        } catch (Exception e) {
            Logs.e(TAG, e.toString());
        }
        return super.toJSONObject();
    }
}
