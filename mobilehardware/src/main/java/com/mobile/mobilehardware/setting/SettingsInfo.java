package com.mobile.mobilehardware.setting;

import com.mobile.mobilehardware.utils.Logs;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.app.Context;
import ohos.sysappcomponents.settings.SystemSettings;
import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
class SettingsInfo {
    private static final String TAG = SettingsInfo.class.getSimpleName();

    static ZSONObject getMobSettings(Context context) {
        SettingsBean settingsBean = new SettingsBean();
        try {
            DataAbilityHelper dataAbilityHelper = DataAbilityHelper.creator(context);
            settingsBean.setScreenOffTimeout(SystemSettings.getValue(dataAbilityHelper, SystemSettings.Display.SCREEN_OFF_TIMEOUT));
            settingsBean.setSoundEffectsEnabled(SystemSettings.getValue(dataAbilityHelper, SystemSettings.Sound.SOUND_EFFECTS_STATUS));
            settingsBean.setScreenBrightnessMode(SystemSettings.getValue(dataAbilityHelper, SystemSettings.Display.AUTO_SCREEN_BRIGHTNESS));
            settingsBean.setDevelopmentSettingsEnabled(SystemSettings.getValue(dataAbilityHelper, SystemSettings.General.DEVELOPMENT_SETTINGS_STATUS));
            settingsBean.setAccelerometerRotation(SystemSettings.getValue(dataAbilityHelper, SystemSettings.General.ACCELEROMETER_ROTATION_STATUS));
            settingsBean.setUsbMassStorageEnabled(SystemSettings.getValue(dataAbilityHelper, SystemSettings.General.HDC_STATUS));
        } catch (Exception e) {
            Logs.e(TAG, e.toString());

        }
        return settingsBean.toJSONObject();
    }

}
