package com.mobile.mobilehardware.battery;

import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
public class BatteryHelper extends BatteryInfo {

    /**
     * 获取电池信息
     *
     * @return 电池JSON
     */
    public static ZSONObject mobGetBattery() {
        return getBattery();
    }

}
