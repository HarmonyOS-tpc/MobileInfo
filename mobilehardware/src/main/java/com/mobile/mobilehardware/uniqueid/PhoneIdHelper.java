package com.mobile.mobilehardware.uniqueid;

import java.util.UUID;

import static com.mobile.mobilehardware.build.BuildInfo.getPropValue;
import static com.mobile.mobilehardware.root.RootHelper.propertiesReader;

/**
 * @author guxiaonian
 */
public class PhoneIdHelper {
    /**
     * 返回自己生产的唯一表示
     * @return unique id of phone
     */
    public static String getUniqueID() {
        String[] properties = propertiesReader();
        String m_szDevIDShort = "35" +
                (getPropValue(properties, "ro.product.board").length() % 10)
                + (getPropValue(properties, "ro.product.brand").length() % 10)
                + (getPropValue(properties, "ro.product.cpu.abi").length() % 10)
                + (getPropValue(properties, "ro.product.device").length() % 10)
                + (getPropValue(properties, "ro.product.manufacturer").length() % 10)
                + (getPropValue(properties, "ro.product.model").length() % 10)
                + (getPropValue(properties, "ro.product.name").length() % 10);

        String serial = getPropValue(properties, "ro.serialno");

        return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
    }
}
