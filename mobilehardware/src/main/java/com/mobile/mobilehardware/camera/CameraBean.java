package com.mobile.mobilehardware.camera;

import com.mobile.mobilehardware.base.BaseBean;
import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobilehardware.utils.Logs;
import ohos.utils.zson.ZSONArray;
import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
public class CameraBean extends BaseBean {
    private static final String TAG = CameraBean.class.getSimpleName();

    /**
     * 摄像头信息
     */
    private ZSONArray cameraInfo;

    public ZSONArray getCameraInfo() {
        return cameraInfo;
    }

    public void setCameraInfo(ZSONArray cameraInfo) {
        this.cameraInfo = cameraInfo;
    }

    @Override
    protected ZSONObject toJSONObject() {
        try {
            jsonObject.put(BaseData.Camera.CAMERA_INFO, cameraInfo);
        } catch (Exception e) {
            Logs.e(TAG, e.toString());
        }
        return super.toJSONObject();
    }

    public static class CameraInfoBean extends BaseBean {

        /**
         * 摄像头的位置
         */
        private String cameraFacing;

        /**
         * 摄像头支持的格式
         */
        private ZSONArray outputFormats;

        /**
         * 是否有闪光灯
         */
        private boolean cameraFlashInfo;

        /**
         * 本相机设备支持的自动曝光模式列表
         */
        private ZSONArray aeAvailableModes;

        /**
         * 相机设备支持的帧频范围列表
         */
        private ZSONArray fpsRanges;

        /**
         * 相机设备支持的自动对焦（AF）模式列表
         */
        private ZSONArray afAvailableModes;

        /**
         * 本相机设备支持的视频稳定模式列表
         */
        private ZSONArray videoStabilizationModes;

        /**
         * 本相机设备支持的自动白平衡模式列表
         */
        private ZSONArray awbAvailableModes;

        private int requestPartialResultCount;
        private float scalerAvailableMaxDigitalZoom;
        private ZSONArray availableFaceDetectModes;
        private int sensorOrientation;

        public ZSONArray getAvailableFaceDetectModes() {
            return availableFaceDetectModes;
        }

        public void setAvailableFaceDetectModes(ZSONArray availableFaceDetectModes) {
            this.availableFaceDetectModes = availableFaceDetectModes;
        }

        public int getSensorOrientation() {
            return sensorOrientation;
        }

        public void setSensorOrientation(int sensorOrientation) {
            this.sensorOrientation = sensorOrientation;
        }

        public void setAwbAvailableModes(ZSONArray awbAvailableModes) {
            this.awbAvailableModes = awbAvailableModes;
        }

        public ZSONArray getVideoStabilizationModes() {
            return videoStabilizationModes;
        }

        public void setVideoStabilizationModes(ZSONArray videoStabilizationModes) {
            this.videoStabilizationModes = videoStabilizationModes;
        }

        public ZSONArray getAfAvailableModes() {
            return afAvailableModes;
        }

        public void setAfAvailableModes(ZSONArray afAvailableModes) {
            this.afAvailableModes = afAvailableModes;
        }

        public ZSONArray getFpsRanges() {
            return fpsRanges;
        }

        public void setFpsRanges(ZSONArray fpsRanges) {
            this.fpsRanges = fpsRanges;
        }

        public ZSONArray getAeAvailableModes() {
            return aeAvailableModes;
        }

        public void setAeAvailableModes(ZSONArray aeAvailableModes) {
            this.aeAvailableModes = aeAvailableModes;
        }

        public boolean isCameraFlashInfo() {
            return cameraFlashInfo;
        }

        public void setCameraFlashInfo(boolean cameraFlashInfo) {
            this.cameraFlashInfo = cameraFlashInfo;
        }

        public String getCameraFacing() {
            return cameraFacing;
        }

        public void setCameraFacing(String cameraFacing) {
            this.cameraFacing = cameraFacing;
        }

        public ZSONArray getOutputFormats() {
            return outputFormats;
        }

        public void setOutputFormats(ZSONArray outputFormats) {
            this.outputFormats = outputFormats;
        }

        public float getScalerAvailableMaxDigitalZoom() {
            return scalerAvailableMaxDigitalZoom;
        }

        public void setScalerAvailableMaxDigitalZoom(float scalerAvailableMaxDigitalZoom) {
            this.scalerAvailableMaxDigitalZoom = scalerAvailableMaxDigitalZoom;
        }

        public int getRequestPartialResultCount() {
            return requestPartialResultCount;
        }

        public void setRequestPartialResultCount(int requestPartialResultCount) {
            this.requestPartialResultCount = requestPartialResultCount;
        }

        @Override
        protected ZSONObject toJSONObject() {
            try {
                jsonObject.put(BaseData.Camera.CameraInfo.CAMERA_FACING, isEmpty(cameraFacing));
                jsonObject.put(BaseData.Camera.CameraInfo.CAMERA_FLASH_INFO, cameraFlashInfo);
                jsonObject.put(BaseData.Camera.CameraInfo.OUTPUT_FORMATS, outputFormats);
                jsonObject.put("aeAvailableModes", aeAvailableModes);
                jsonObject.put("afAvailableModes", afAvailableModes);
                jsonObject.put("videoStabilizationModes", videoStabilizationModes);
                jsonObject.put("awbAvailableModes", awbAvailableModes);
                jsonObject.put("requestPartialResultCount",requestPartialResultCount);
                jsonObject.put("scalerAvailableMaxDigitalZoom",scalerAvailableMaxDigitalZoom);
                jsonObject.put("sensorOrientation",sensorOrientation);
                jsonObject.put("availableFaceDetectModes", availableFaceDetectModes);
                jsonObject.put("fpsRanges", fpsRanges);

            } catch (Exception e) {
                Logs.e(TAG, e.toString());
            }
            return super.toJSONObject();
        }
    }
}
