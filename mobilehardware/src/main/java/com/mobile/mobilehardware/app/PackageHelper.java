package com.mobile.mobilehardware.app;


import com.mobile.mobilehardware.MobileHardWareHelper;

import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
public class PackageHelper extends PackageInfo {


    /**
     * 获取包类信息
     *
     * @return 包信息
     */
    public static ZSONObject getPackageInfo() {
        return packageInfo(MobileHardWareHelper.getContext());
    }
}
