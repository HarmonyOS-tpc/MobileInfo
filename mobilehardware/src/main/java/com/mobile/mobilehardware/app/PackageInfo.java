package com.mobile.mobilehardware.app;

import com.mobile.mobilehardware.utils.Logs;
import ohos.app.Context;
import ohos.bundle.ApplicationInfo;
import ohos.bundle.BundleInfo;
import ohos.bundle.IBundleManager;
import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
class PackageInfo {

    private static final String TAG = PackageInfo.class.getSimpleName();

    static ZSONObject packageInfo(Context context) {
        PackageBean packageBean = new PackageBean();
        try {
            IBundleManager packageManager = context.getBundleManager();
            ApplicationInfo applicationInfo=context.getApplicationInfo();
            packageBean.setAppName(applicationInfo.getLabel());
            BundleInfo packageInfo = packageManager.getBundleInfo(context.getBundleName(), 0);
            String packageName = "unknown";
            if (packageInfo != null)
                packageName = packageInfo.name;
            packageBean.setPackageName(packageName);
            packageBean.setDescription(applicationInfo.getDescription());
            packageBean.setIcon(applicationInfo.getIcon());
            packageBean.setAppVersionCode(packageInfo.getVersionCode());
            packageBean.setAppVersionName(packageInfo.getVersionName());
            packageBean.setTargetSdkVersion(packageInfo.getTargetVersion());
            packageBean.setMinSdkVersion(packageInfo.getMinSdkVersion());
            packageBean.setLastUpdateTime(packageInfo.getUpdateTime());
            packageBean.setFirstInstallTime(packageInfo.getInstallTime());
        } catch (Exception e) {
            Logs.e(TAG, e.toString());
        }
        return packageBean.toJSONObject();
    }
}
