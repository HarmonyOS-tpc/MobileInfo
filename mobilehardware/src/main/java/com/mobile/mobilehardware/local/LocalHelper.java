package com.mobile.mobilehardware.local;

import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
public class LocalHelper extends LocalInfo {

    public static ZSONObject mobGetMobLocal() {
        return getMobLocal();
    }
}
