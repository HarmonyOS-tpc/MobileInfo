package com.mobile.mobilehardware.useragent;

import com.mobile.mobilehardware.MobileHardWareHelper;
import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobilehardware.utils.TextUtil;
import ohos.agp.components.webengine.WebConfig;
import ohos.agp.components.webengine.WebView;

public class UserAgentHelper {
    public static String getDefaultUserAgent() {
        String userAgent = null;
        try {
            WebView webView = new WebView(MobileHardWareHelper.getContext());
            WebConfig webConfig = webView.getWebConfig();
            userAgent = webConfig.getUserAgent();
            if (userAgent == null) {
                userAgent = System.getProperty("http.agent");
            }
        } catch (Exception localException) {

        }
        return TextUtil.isEmpty(userAgent) ? BaseData.UNKNOWN_PARAM : userAgent;
    }
}
