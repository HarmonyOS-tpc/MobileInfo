package com.mobile.mobilehardware.simcard;


import com.mobile.mobilehardware.MobileHardWareHelper;
import ohos.utils.zson.ZSONObject;


/**
 * @author guxiaonian
 */
public class SimCardHelper extends SimCardInfo {

    /**
     * 获取网络卡信息
     *
     * @return 运营商返回
     */
    public static ZSONObject mobileSimInfo() {
        return getMobSimInfo(MobileHardWareHelper.getContext());
    }
}
