package com.mobile.mobilehardware.simcard;

import com.mobile.mobilehardware.utils.TextUtil;
import ohos.app.Context;
import ohos.telephony.RadioInfoManager;

import java.util.regex.Pattern;

/**
 * @author guxiaonian
 */
public class MidInfo {
    private static Pattern pattern = Pattern.compile("[0-9]*");

    public static boolean isNumeric(String str) {

        return pattern.matcher(str).matches();
    }
}
