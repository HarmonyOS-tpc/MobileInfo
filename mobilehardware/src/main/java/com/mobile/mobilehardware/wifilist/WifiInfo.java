package com.mobile.mobilehardware.wifilist;


import ohos.app.Context;
import ohos.bundle.IBundleManager;
import ohos.event.commonevent.CommonEventData;
import ohos.event.commonevent.CommonEventManager;
import ohos.event.commonevent.CommonEventSubscribeInfo;
import ohos.event.commonevent.CommonEventSubscriber;
import ohos.event.commonevent.MatchingSkills;
import ohos.rpc.RemoteException;
import ohos.utils.zson.ZSONArray;
import ohos.wifi.WifiDevice;
import ohos.wifi.WifiEvents;
import ohos.wifi.WifiScanInfo;

import java.util.List;

/**
 * @author guxiaonian
 */
class WifiInfo {
    private static CommonEventSubscriber wifiCommonEventSubscriber;

    static void getWifiList(final Context context, final WifiScanListener wifiScanListener) {
        if (wifiScanListener == null) {
            throw new NullPointerException("the WifiScanListener is null");
        }
        final long startTime = System.currentTimeMillis();
        final WifiDevice wifiManager = WifiDevice.getInstance(context);
        final WifiBean wifiBean = new WifiBean();
        if (wifiManager == null) {
            wifiScanListener.onResult(wifiBean.toJSONObject());
        }
        if (context.verifySelfPermission("ohos.permission.LOCATION") != IBundleManager.PERMISSION_GRANTED) {
            wifiScanListener.onResult(wifiBean.toJSONObject());
        }
        MatchingSkills match = new MatchingSkills();
        // Add listening for WLAN state changes.
        match.addEvent(WifiEvents.EVENT_SCAN_STATE);
        CommonEventSubscribeInfo subscribeInfo = new CommonEventSubscribeInfo(match);
        wifiCommonEventSubscriber = new CommonEventSubscriber(subscribeInfo){
            @Override
            public void onReceiveEvent(CommonEventData commonEventData) {
                try {
                    CommonEventManager.unsubscribeCommonEvent(wifiCommonEventSubscriber);
                    scanSuccess(wifiManager, wifiBean, startTime, wifiScanListener);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        };
        try {
            CommonEventManager.subscribeCommonEvent(wifiCommonEventSubscriber);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        wifiManager.scan();
    }

    private static void scanSuccess(WifiDevice wifiManager, WifiBean wifiBean, long startTime, WifiScanListener wifiScanListener) {
        List<WifiScanInfo> results = wifiManager.getScanInfoList();
        wifiBean.setWifiScanStatus(results.size() != 0);
        ZSONArray scanArray = new ZSONArray();
        for (WifiScanInfo scanResult : results) {
            WifiBean.WifiResultBean wifiResultBean = new WifiBean.WifiResultBean();
            wifiResultBean.setBSSID(scanResult.getBssid());
            wifiResultBean.setSSID(scanResult.getSsid());
            wifiResultBean.setCapabilities(scanResult.getCapabilities());
            wifiResultBean.setLevel(scanResult.getRssi());
            scanArray.add(wifiResultBean.toJSONObject());
        }
        wifiBean.setWifiScanResult(scanArray);
        wifiBean.setTime(System.currentTimeMillis() - startTime);
        wifiScanListener.onResult(wifiBean.toJSONObject());
    }
}
