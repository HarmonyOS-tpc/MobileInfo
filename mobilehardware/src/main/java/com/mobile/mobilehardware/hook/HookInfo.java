package com.mobile.mobilehardware.hook;

import com.mobile.mobilehardware.MobileNativeHelper;
import com.mobile.mobilehardware.utils.Logs;
import com.mobile.mobilehardware.utils.CommandUtil;
import ohos.aafwk.ability.RunningProcessInfo;
import ohos.app.Context;
import ohos.utils.zson.ZSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author guxiaonian
 */
class HookInfo {
    private static final String TAG = HookInfo.class.getSimpleName();

    /**
     * 判断是否有xposed等hook工具
     *
     * @param context Application Context
     * @return json object with xposed hook values
     */
    static ZSONObject getXposedHook(Context context) {
        HookBean hookBean = new HookBean();
        HookBean.XposedBean xposedBean = new HookBean.XposedBean();
        HookBean.SubstrateBean substrateBean = new HookBean.SubstrateBean();
        HookBean.FridaBean fridaBean = new HookBean.FridaBean();
        try {
            chargeXposedHookMethod(xposedBean, substrateBean);
            chargeXposedJars(xposedBean, substrateBean, fridaBean);
            fridaBean.setCheckRunningProcesses(checkRunningProcesses(context));
            addMethod(xposedBean);
        } catch (Exception e) {
            Logs.e(TAG, e.toString());
        }

        substrateBean.setcSo(MobileNativeHelper.checkSubstrateBySo() == 1);
        String mapData = MobileNativeHelper.checkHookByMap();
        String packageData = MobileNativeHelper.checkHookByPackage();
        if (mapData != null && mapData.length() > 0) {
            if (mapData.contains("xposed")) {
                xposedBean.setcMap(true);
            }
            if (mapData.contains("frida")) {
                fridaBean.setcMap(true);
            }
            if (mapData.contains("substrate")) {
                substrateBean.setcMap(true);
            }
        }
        if (packageData != null && packageData.length() > 0) {
            if (packageData.contains("xposed")) {
                xposedBean.setcPackage(true);
            }
            if (packageData.contains("substrate")) {
                substrateBean.setcPackage(true);
            }
        }

        hookBean.setIsHaveXposed(xposedBean.toJSONObject());
        hookBean.setIsHaveSubstrate(substrateBean.toJSONObject());
        hookBean.setIsHaveFrida(fridaBean.toJSONObject());
        return hookBean.toJSONObject();
    }

    private static boolean checkRunningProcesses(Context context) {
        boolean returnValue = false;
        // Get currently running application processes
        List<RunningProcessInfo> list = context.getAbilityManager().getAllRunningProcesses();
        if (list != null) {
            String tempName;
            for (int i = 0; i < list.size(); ++i) {
                tempName = list.get(i).getProcessName();
                if (tempName.contains("fridaserver")) {
                    returnValue = true;
                }
            }
        }
        return returnValue;
    }

    /**
     * 新增方法
     * @param xposedBean xposed storage bean
     * 是否安装了Xposed
     */
    private static void addMethod( HookBean.XposedBean xposedBean) {
        xposedBean.setCheckNativeMethod(checkNativeMethod());
        xposedBean.setCheckSystem(checkSystem());
        xposedBean.setCheckExecLib(checkExecLib());
        xposedBean.setCheckXposedBridge(checkXposedBridge());
    }

    /**
     * 新增判断系统方法调用钩子
     * 方法参考自<url>https://github.com/w568w/XposedChecker/</url>
     *
     * @return 是否安装了Xposed
     */
    private static boolean checkNativeMethod() {
        try {
            Method method = Throwable.class.getDeclaredMethod("getStackTrace");
            return Modifier.isNative(method.getModifiers());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 新增虚拟检测Xposed环境
     * 方法参考自<url>https://github.com/w568w/XposedChecker/</url>
     *
     * @return 是否安装了Xposed
     */
    private static boolean checkSystem() {
        return System.getProperty("vxp") != null;
    }

    /**
     * 寻找Xposed运行库文件
     * 方法参考自<url>https://github.com/w568w/XposedChecker/</url>
     *
     * @return 是否安装了Xposed
     */
    private static boolean checkExecLib() {
        String result = CommandUtil.getSingleInstance().exec("ls /system/lib");
        if (result != null && result.length() > 0) {
            return false;
        }
        return result.contains("xposed");
    }


    /**
     * 环境变量特征字判断
     * 方法参考自<url>https://github.com/w568w/XposedChecker/</url>
     *
     * @return 是否安装了Xposed
     */
    private static boolean checkXposedBridge() {
        String result = System.getenv("CLASSPATH");
        if (result != null && result.length() > 0) {
            return false;
        }
        return result.contains("XposedBridge");
    }

    /**
     * 检测调用栈中的可疑方法
     * @param substrateBean substrate storage bean
     * @param xposedBean xposed storage bean
     */
    private static void chargeXposedHookMethod(HookBean.XposedBean xposedBean, HookBean.SubstrateBean substrateBean) {

        try {
            throw new Exception("Deteck hook");
        } catch (Exception e) {

            int zygoteInitCallCount = 0;
            for (StackTraceElement item : e.getStackTrace()) {
                if ("com.saurik.substrate.MS$2".equals(item.getClassName()) && "invoke".equals(item.getMethodName())) {
                    substrateBean.setCheckSubstrateHookMethod(true);
                }
            }
        }
    }

    /**
     * 检测内存中可疑的jars
     * @param fridaBean frida storage bean
     * @param substrateBean Substrate storage bean
     * @param xposedBean xposed storage bean
     */
    private static void chargeXposedJars(HookBean.XposedBean xposedBean, HookBean.SubstrateBean substrateBean, HookBean.FridaBean fridaBean) {
        Set<String> libraries = new HashSet<String>();
        String mapsFilename = "/proc/" + ohos.os.ProcessManager.getPid() + "/maps";
        try {
            BufferedReader reader = new BufferedReader(new FileReader(mapsFilename));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.toLowerCase().contains("frida")) {
                    fridaBean.setCheckFridaJars(true);
                }
                if (line.endsWith(".so") || line.endsWith(".jar")) {
                    int n = line.lastIndexOf(" ");
                    libraries.add(line.substring(n + 1));
                }


            }
            for (String library : libraries) {
                if (library.contains("com.saurik.substrate")) {
                    substrateBean.setCheckSubstrateJars(true);
                }
                if (library.contains("XposedBridge.jar")) {
                    xposedBean.setCheckXposedJars(true);
                }
            }

            reader.close();
        } catch (Exception e) {
        }
    }
}
