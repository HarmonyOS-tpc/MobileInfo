package com.mobile.mobilehardware.build;

import ohos.app.Context;
import ohos.utils.zson.ZSONObject;

import static com.mobile.mobilehardware.root.RootHelper.propertiesReader;

/**
 * @author guxiaonian
 */
public class BuildInfo {
    private static final String TAG = BuildInfo.class.getSimpleName();

    static ZSONObject getBuildInfo(Context context) {
        BuildBean buildBean = new BuildBean();
        String[] properties = propertiesReader();
        buildBean.setBoard(getPropValue(properties, "ro.product.board"));
        buildBean.setBootloader(getPropValue(properties, "ro.bootloader"));
        buildBean.setBrand(getPropValue(properties, "ro.product.brand"));
        buildBean.setDevice(getPropValue(properties, "ro.product.device"));
        buildBean.setDisplay(getPropValue(properties, "ro.build.display.id"));
        buildBean.setFingerprint(getPropValue(properties, "ro.build.fingerprint"));
        buildBean.setHardware(getPropValue(properties, "ro.hardware"));
        buildBean.setId(getPropValue(properties, "ro.build.id"));
        buildBean.setManufacturer(getPropValue(properties, "ro.product.manufacturer"));
        buildBean.setModel(getPropValue(properties, "ro.product.model"));
        buildBean.setProduct(getPropValue(properties, "ro.product.name"));
        buildBean.setRadio(getPropValue(properties, "gsm.version.baseband"));
        buildBean.setSerial(getPropValue(properties, "ro.serialno"));
        buildBean.setTags(getPropValue(properties, "ro.build.tags"));
        buildBean.setTime(Long.parseLong(getPropValue(properties, "ro.build.date.utc")));
        buildBean.setType(getPropValue(properties, "ro.build.type"));
        buildBean.setUser(getPropValue(properties, "ro.build.user"));
        buildBean.setOsVersion(getPropValue(properties, "ro.build.version.base_os"));
        buildBean.setPreviewSdkInt(Integer.parseInt(getPropValue(properties, "ro.build.version.preview_sdk")));
        buildBean.setSecurityPatch(getPropValue(properties, "ro.huawei.build.version.security_patch"));
        buildBean.setReleaseVersion(getPropValue(properties, "ro.build.version.release"));
        buildBean.setIncremental(getPropValue(properties, "ro.build.version.incremental"));
        buildBean.setSdkInt(Integer.parseInt(getPropValue(properties, "ro.build.version.sdk")));
        ZSONObject jsonObject = buildBean.toJSONObject();
        jsonObject.put("wifi.interface", getPropValue(properties, "wifi.interface"));
        jsonObject.put("gsm.sim.state", getPropValue(properties, "gsm.sim.state"));
        jsonObject.put("gsm.version.baseband", getPropValue(properties, "gsm.version.baseband"));
        jsonObject.put("gsm.version.ril-impl", getPropValue(properties, "gsm.version.ril-impl"));
        jsonObject.put("net.hostname", getPropValue(properties, "net.hostname"));
        jsonObject.put("ro.board.platform", getPropValue(properties, "ro.board.platform"));
        jsonObject.put("ro.build.characteristics", getPropValue(properties, "ro.build.characteristics"));
        jsonObject.put("ro.build.date", getPropValue(properties, "ro.build.date"));
        jsonObject.put("ro.build.description", getPropValue(properties, "ro.build.description"));
        jsonObject.put("ro.boot.serialno", getPropValue(properties, "ro.boot.serialno"));
        jsonObject.put("ro.opengles.version", getPropValue(properties, "ro.opengles.version"));
        jsonObject.put("ro.product.cpu.abi", getPropValue(properties, "ro.product.cpu.abi"));
        jsonObject.put("gsm.current.phone-type", getPropValue(properties, "gsm.current.phone-type"));
        jsonObject.put("gsm.network.type", getPropValue(properties, "gsm.network.type"));
        jsonObject.put("gsm.operator.isroaming", getPropValue(properties, "gsm.operator.isroaming"));
        jsonObject.put("ro.adb.secure", getPropValue(properties, "ro.adb.secure"));
        jsonObject.put("ro.carrier", getPropValue(properties, "ro.carrier"));
        jsonObject.put("ro.secure", getPropValue(properties, "ro.secure"));
        jsonObject.put("ro.debuggable", getPropValue(properties, "ro.debuggable"));
        jsonObject.put("rild.libargs", getPropValue(properties, "rild.libargs"));
        jsonObject.put("persist.sys.timezone", getPropValue(properties, "persist.sys.timezone"));
        jsonObject.put("persist.sys.country", getPropValue(properties, "persist.sys.country"));
        jsonObject.put("gsm.operator.numeric", getPropValue(properties, "gsm.operator.numeric"));
        jsonObject.put("wlan.driver.status", getPropValue(properties, "wlan.driver.status"));
        jsonObject.put("ro.runtime.firstboot", getPropValue(properties, "ro.runtime.firstboot"));
        jsonObject.put("init.svc.adbd", getPropValue(properties, "init.svc.adbd"));
        jsonObject.put("ro.serialno", getPropValue(properties, "ro.serialno"));
        jsonObject.put("keyguard.no_require_sim", getPropValue(properties, "keyguard.no_require_sim"));
        jsonObject.put("ro.bt.bdaddr_path", getPropValue(properties, "ro.bt.bdaddr_path"));
        return jsonObject;
    }

    public static String getPropValue(String[] props, String key) {
        String keystr = "[" + key + "]";
        for (String line : props) {
            if (line.contains(keystr)) {
                String val = line.split("]: ")[1];
                return val.substring(1, val.length()-1);
            }
        }
        return "UNKNOWN";
    }
}
