package com.mobile.mobilehardware.build;

import ohos.app.Context;
import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
public class BuildHelper extends BuildInfo {

    /**
     * build信息
     * @param context application context
     * @return build info
     */
    public static ZSONObject mobGetBuildInfo(Context context) {
        return getBuildInfo(context);
    }
}
