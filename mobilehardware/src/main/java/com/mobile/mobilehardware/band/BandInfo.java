package com.mobile.mobilehardware.band;

import com.mobile.mobilehardware.MobileNativeHelper;
import com.mobile.mobilehardware.utils.Logs;
import com.mobile.mobilehardware.utils.TextUtil;
import ohos.utils.zson.ZSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * @author guxiaonian
 */
@SuppressWarnings("ALL")
class BandInfo {
    private static final String TAG = BandInfo.class.getSimpleName();

    /**
     * bandInfo
     *
     * @return Band info
     */
    static ZSONObject getBandInfo() {
        BandBean bandBean = new BandBean();
        try {
            String[] lines = propertiesReader();
            bandBean.setBaseBand(getPropValue(lines, "gsm.version.baseband"));
            bandBean.setInnerBand(getPropValue(lines, "ro.build.version.incremental"));
            String linuxBand = getLinuxKernalInfo();
            bandBean.setLinuxBand(TextUtil.isEmpty(linuxBand) ? System.getProperty("os.version") : linuxBand);
            bandBean.setDetailLinuxBand(MobileNativeHelper.kennel());
        } catch (Exception e) {
            Logs.e(TAG, e.toString());
        }
        return bandBean.toJSONObject();
    }

    /**
     * Used for existingDangerousProperties().
     *
     * @return - list of system properties
     */
    private static String[] propertiesReader() {
        InputStream inputstream = null;
        try {
            inputstream = Runtime.getRuntime().exec("getprop").getInputStream();
        } catch (IOException e) {
            Logs.i(TAG, e.toString());
        }
        if (inputstream == null) {
            return null;
        }

        String allProperties = "";
        try {
            allProperties = new Scanner(inputstream).useDelimiter("\\A").next();
        } catch (NoSuchElementException e) {
            Logs.i(TAG, e.toString());
        }
        return allProperties.split("\n");
    }

    private static String getPropValue(String[] props, String key) {
        String keystr = "[" + key + "]";
        for (String line : props) {
            if (line.contains(keystr)) {
                String val = line.split("]: ")[1];
                return val.substring(1, val.length() - 1);
            }
        }
        return "UNKNOWN";
    }

    /***
     * 获取OpenHarmony Linux内核版本信息
     * @return linux kernal info
     */
    private static String getLinuxKernalInfo() {
        Process process = null;
        String mLinuxKernal;
        try {
            process = Runtime.getRuntime().exec("cat /proc/version");
        } catch (IOException e) {
            Logs.i(TAG, e.toString());
        }

        if (process != null) {
            InputStream outs = process.getInputStream();
            InputStreamReader isrout = new InputStreamReader(outs);
            BufferedReader brout = new BufferedReader(isrout, 8 * 1024);

            StringBuilder result = new StringBuilder();
            String line;
            // get the whole standard output string
            try {
                while ((line = brout.readLine()) != null) {
                    result.append(line);
                }
            } catch (IOException e) {
                Logs.i(TAG, e.toString());
            }

            if (!"".equals(result.toString())) {
                String keyword = "version ";
                int index = result.indexOf(keyword);
                line = result.substring(index + keyword.length());
                index = line.indexOf(" ");
                mLinuxKernal = line.substring(0, index);
                return mLinuxKernal;
            }
        }
        return null;
    }

}
