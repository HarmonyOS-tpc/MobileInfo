package com.mobile.mobilehardware.emulator;

import com.mobile.mobilehardware.base.BaseBean;
import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobilehardware.utils.Logs;
import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
public class EmulatorBean extends BaseBean {
    private static final String TAG = EmulatorBean.class.getSimpleName();

    /**
     * build
     */
    private boolean checkBuild;

    /**
     * 管道检测
     */
    private boolean checkPipes;

    /**
     * 驱动程序检测
     */
    private boolean checkQEmuDriverFile;


    /**
     * cpu架构检测
     */
    private boolean checkCpuInfo;

    public boolean isCheckBuild() {
        return checkBuild;
    }

    public void setCheckBuild(boolean checkBuild) {
        this.checkBuild = checkBuild;
    }

    public boolean isCheckPipes() {
        return checkPipes;
    }

    public void setCheckPipes(boolean checkPipes) {
        this.checkPipes = checkPipes;
    }

    public boolean isCheckQEmuDriverFile() {
        return checkQEmuDriverFile;
    }

    public void setCheckQEmuDriverFile(boolean checkQEmuDriverFile) {
        this.checkQEmuDriverFile = checkQEmuDriverFile;
    }

    public boolean isCheckCpuInfo() {
        return checkCpuInfo;
    }

    public void setCheckCpuInfo(boolean checkCpuInfo) {
        this.checkCpuInfo = checkCpuInfo;
    }

    @Override
    protected ZSONObject toJSONObject() {
        try {
            jsonObject.put(BaseData.Emulator.CHECK_BUILD, checkBuild);
            jsonObject.put(BaseData.Emulator.CHECK_PIPES, checkPipes);
            jsonObject.put(BaseData.Emulator.CHECK_QEMU_DRIVER_FILE, checkQEmuDriverFile);
            jsonObject.put(BaseData.Emulator.CHECK_CPU_INFO, checkCpuInfo);

        } catch (Exception e) {
            Logs.e(TAG, e.toString());
        }
        return super.toJSONObject();
    }
}
