MobileInfo
==================

## Introduction

<div align="center">
**<mark>Openharmony</mark> Hardware Information**

**请自觉遵循《信息安全技术移动互联网应用(App)收集个人信息基本规范(草案)》**
</div>
<br>

Library fetches mobile hardware info like BAND, BLUETOOTH, BUILD, CPU, LOCAL, SCREEN, SETTING, APPLIST, DEBUG, EMULATOR, NETWORK, SIGNAL, APP, HOOK, MOREOPEN, COMPLETE, RANDOM, AUDIO, PHONEID, SIMCARD, BATTERY, MEMORY, SDCARD, ROOT, USERAGENT and CAMERA.

![](./snapshots/mobileinfo.gif)

## Usage Instructions

To read the mobile info, for example for ID
```java
    List<Param> list = getListParam("uniqueID", PhoneIdHelper.getUniqueID());
```

## Installation Instructions
```
Method 1. Add following dependencies in entry/build.gradle to generate mobileinfo library har:
    dependencies {
      implementation project(':mobilehardware')
    }

Method 2. Add mobile library har file to entry/libs and add below line to include har file as dependency in entry/build.gradle file.

	dependencies {
		implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	}
```



## License

[License Apache](http://www.apache.org/licenses/LICENSE-2.0)
